<?php

/**
 * @file
 * Admin page callbacks for feedapi taxonomy compare module
 */

/**
 * Form builder.  Configure feedapi taxonomy compare
 *
 * @inggroup forms
 * @see system_settings_form()
 */
function feedapi_taxonomy_compare_admin_settings() {
  //get list of vocabularies
  foreach (taxonomy_get_vocabularies() as $voc) {
    $options[$voc->vid] = $voc->name;
  }

  $form['feedapi_taxonomy_compare_vocabs_compare'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Vocabularies to Compare To'),
    '#options' => $options,
    '#default_value' => variable_get('feedapi_taxonomy_compare_vocabs_compare', array('0')),
    '#multiple' => TRUE,
  );

  return system_settings_form($form);
}