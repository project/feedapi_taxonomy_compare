<?php

/**
 * @file
 * Admin page callbacks for feedapi taxonomy compare module
 */

/**
 * Form builder.  Configure feedapi taxonomy compare
 *
 * @inggroup forms
 * @see system_settings_form()
 */
function feedapi_taxonomy_admin_settings() {
  //get list of vocabularies
  foreach (taxonomy_get_vocabularies() as $voc) {
    $options[$voc->vid] = $voc->name;
  }
  
	$form['feedapi_taxonomy_filter'] = array(
	 '#type' => 'fieldset',
	 '#title' => t('FeedAPI Taxonomy Filter Setttings'),
	 '#collapsible' => TRUE,
	 '#collapsed' => FALSE,
	);
	
  $form['feedapi_taxonomy_filter']['feedapi_taxonomy_filter_vocabs_compare'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Vocabularies to Compare To'),
    '#options' => $options,
    '#default_value' => variable_get('feedapi_taxonomy_filter_vocabs_compare', array('0')),
    '#multiple' => TRUE,
  );
	
  $form['feedapi_taxonomy_filter']['feedapi_taxonomy_remvoe_terms'] = array(
    '#type' => 'checkboxe',
    '#title' => t('Only Add matched Terms'),
		'#description' => t('This will remove all of the terms that were not matched'),
    '#default_value' => variable_get('feedapi_taxonomy_remvoe_terms', FALSE),
  );
	
  $form['feedapi_taxonomy_filter']['feedapi_taxonomy_remove_node'] = array(
    '#type' => 'checkboxe',
    '#title' => t('Remove unmatched Nodes'),
    '#description' => t('This will remove a node if the term does not match'),
    '#default_value' => variable_get('feedapi_taxonomy_remove_node', FALSE),
  );

  return system_settings_form($form);
}